(function () {
  'use strict';

  angular
      .module('simility')
      .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
          url: '/:genre?/{page:.*}/',
          templateUrl: 'app/movies/partials/movies.html',
          controller: 'MoviesController',
          controllerAs: 'vm'
        })
        .state('details', {
          url: '/movie/{id:.*}',
          templateUrl: 'app/movies/partials/movie-details.html',
          controller: 'MovieDetailsController',
          controllerAs: 'vm'
        });

    $urlRouterProvider.otherwise('/');
  }

})();
