(function () {
  'use strict';

  angular
      .module('simility',
      ['ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'ui.router',
        'toastr',
        'angular-inview',
        'ngdexie',
        'ngdexie.ui',
        'ngLodash'
      ]);

})();
