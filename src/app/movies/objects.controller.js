(function () {
  'use strict';

  angular
      .module('simility')
      .controller('MoviesController', MoviesController);


  MoviesController.$inject = ['$scope', '$state','$stateParams', '$timeout', '$document', 'toastr', 'lodash', 'moviesService']


  function MoviesController($scope, $state,$stateParams, $timeout, $document, toastr, _, moviesService) {
    var vm = this;
    vm.movies = [];
    vm.pageCount = null
    vm.genres = []
    vm.selectedGenre = null
    vm.sortOrder = 1
    vm.toggleSortOrder = toggleSortOrder
    vm.start = 0
    vm.perPage = 80
    vm.currentPage = 0
    //vm.getMovies = getMovies

    init()

    /**
     * Set up the initial values
     */
    function init() {
      if ($stateParams.page) {
        vm.start = $stateParams.page * vm.perPage
        vm.currentPage = $stateParams.page
      }
      if ($stateParams.genre) {
        vm.selectedGenre = $stateParams.genre
        loadMoviesByGenre(vm.selectedGenre)
      } else {
        vm.selectedGenre = null
        loadMovies()
      }

      //setPageCount()
    }

    /**
     * Call the service to get the movies by genre
     * @param genre
     */
    function loadMoviesByGenre(genre) {
      moviesService.getObjectsByGenre(genre).then(function (data) {
        vm.movies = data
        vm.pageCount = Math.ceil(vm.movies.length / vm.perPage)
        vm.genres = moviesService.getGenres()

        $timeout(function () {
          console.log('waited for a sec')
          loadPosters()
        }, 1000);

      }, function (error) {

      })
    }

    /**
     * Loads all movies on the first load
     */
    function loadMovies() {
      moviesService.getObjects().then(function (data) {
        vm.movies = data
        vm.pageCount = Math.ceil(vm.movies.length / vm.perPage)
        vm.genres = moviesService.getGenres()
        $timeout(function () {
          console.log('waited for a sec')
          loadPosters()
        }, 1000);

      }, function (error) {

      })
    }

    /**
     * Gets the movie posters from IMBD database
     *
     * Gets the posters visible in the view port. Scroll event is debounced by 500ms
     *
     */
    function loadPosters() {
      angular.element('.isInView.noPoster').each(function (element) {
        var id = angular.element(this).data('id')
        var index = angular.element(this).data('index')
        moviesService.getPoster(id).then(function (data) {
          vm.movies[index] = data
        })
      })

    }


    var debounced = _.debounce(function () {
      loadPosters()
    }, 500); // Executes 500ms after last call of the debounced function.
    angular.element($document).on('scroll', debounced);


    function setPageCount() {
      if (vm.movies) {
        vm.pageCount = Math.ceil(vm.movies.length / vm.perPage)
      }
    }


    function toggleSortOrder() {
      vm.sortOrder = vm.sortOrder ? false : true
    }

    $scope.$watch('vm.search',
        function (newVal, oldVal) {
          if (newVal) {
            vm.perPage = 10000
          }
        })


    $scope.$watch('vm.selectedGenre', function () {
      if (vm.selectedGenre) {
        $state.go('home',{genre:vm.selectedGenre,page:0})
        //loadMoviesByGenre(vm.selectedGenre)
        //vm.currentPage = $stateParams.page =0
      }
    })


  }


})();
