/**
 * Created by satish on 19/02/17.
 */

angular
    .module('simility').filter('range', function() {
      return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
          input.push(i);
        }
        return input;
      };
    });
