(function () {
  'use strict';

  angular
      .module('simility')
      .controller('MovieDetailsController', MovieDetailsController);


  MovieDetailsController.$inject = ['$stateParams', 'moviesService']


  function MovieDetailsController($stateParams, moviesService) {
    var vm = this;
    vm.movieId = $stateParams.id
    vm.movie = {}

    init()

    function init() {
      //@todo
      //If no movie ID, redirect to home
      moviesService.getMovieDetails(vm.movieId).then(function (data) {
        vm.movie = data
      }, function (error) {
        vm.error = error
      })
    }
  }


})();
