/**
 * Created by satish on 19/02/17.
 */
(function () {

  'use strict';
  angular.module('simility').factory('moviesService', moviesService);
  moviesService.$inject = ['$http', '$q', 'ngDexie', 'lodash']
  function moviesService($http, $q, ngDexie, _) {
    var service = {
      getObjects: getObjects,
      getObjectsByGenre: getObjectsByGenre,
      getMovieDetails: getMovieDetails,
      objects: [],
      getPoster: getPoster,
      getGenres: getGenres,
      genres: []
    }

    return service

    function getObjects() {
      var deferred = $q.defer();

      // See if we already have the data in local storage
      ngDexie.list('movies').then(
          function (data) {
            if (data.length) {
              deferred.resolve(data)
              service.objects = data.slice(0,1000)

            } else {
              console.log('calling api')
              getFromApi()
            }

          },
          function (error) {
            getFromApi()
          }
      )

      function getFromApi() {
        $http(
            {
              method: 'GET',
              url: 'http://starlord.hackerearth.com/simility/movieslisting'
            }
        ).then(
            function (response) {
              angular.forEach(response.data, function (movie) {
                movie.id = getMovieId(movie);
                movie.genres = movie.genres.split('|')
                movie.plot_keywords = movie.plot_keywords.split('|')
                movie.budget = movie.budget*1
                delete movie['movie_imdb_link'];

                service.genres = _.union(service.genres, movie.genres)
                debugger
                ngDexie.put('movies', movie).then(function () {
                }, function (err) {
                  console.log(err)
                });
              })

              service.objects = response.data
              deferred.resolve(response.data)
            },

            function (err) {
              var message = "Something is seriously worng with the universe today. We are unable to get your movie list :("
              deferred.reject(message)
            })
      }

      return deferred.promise;

    }


    function getObjectsByGenre(genre) {
      var deferred = $q.defer();

      // See if we already have the data in local storage
      ngDexie.listByIndex('movies', 'genres', genre).then(
          function (data) {
            service.objects = data
            deferred.resolve(data)
          },
          function (error) {
            deferred.reject('Failed')
            // Data not found or could not be retrived from local db
            console.log(error)
            getFromApi()
          }
      )
      return deferred.promise;


    }

    function getMovieDetails(id) {
      var deferred = $q.defer();

      ngDexie.getByIndex('movies', 'id', id).then(function (data) {
        deferred.resolve(data)
      }, function (error) {
        deferred.reject(error)
      });
      return deferred.promise;

    }

    function getGenres() {
      if (!service.genres.length)
        angular.forEach(service.objects, function (object) {
          service.genres = _.union(service.genres, object.genres)
        })
      return service.genres
    }

    function getPoster(id) {
      var deferred = $q.defer();
      var url = 'http://www.omdbapi.com/?i=' + id + '&plot=short&r=json'

      $http(
          {
            method: 'GET',
            url: url
          }
      ).then(function (reposne) {
            ngDexie.getByIndex('movies', 'id', id).then(function (data) {
              data.poster = reposne.data.Poster
              ngDexie.put('movies', data).then(function () {
                console.log('Movie updated');
              })
              deferred.resolve(data)
            });
          },
          function () {
            getObjects()
          }
      )

      return deferred.promise
    }

    function getMovieId(movie) {
      var id = movie.movie_imdb_link.match(/tt[0-9]+/)
      return id[0]
    }

    /**
     * Search a given list of dictionaries by give key:value and return object or value of the return_key
     * @param list
     * @param key
     * @param value
     * @param return_key
     * @returns {*}
     */
    function searchList(list, key, value, return_key) {

      for (var i = 0; i < list.length; i++) {
        if (list[i][key] == value) {
          if (return_key) {
            return list[i][return_key]
          }
          else {
            return list[i]
          }
        }
      }
    }
  }
})();
