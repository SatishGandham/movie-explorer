(function () {
  'use strict';

  angular
      .module('simility')
      .config(config)
      .config(ngDexieConfig);

  /** @ngInject */
  function config($logProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
  }


  function ngDexieConfig(ngDexieProvider) {
    ngDexieProvider.setOptions({name: 'simility', debug: false});
    ngDexieProvider.setConfiguration(function (db) {
      db.version(455).stores({
        movies: "id,*movie_title,director_name,poster, actor_1_name, actor_2_name, *genres, language, country, content_rating, *budget, *title_year, plot_keywords",
      });
      db.on('error', function (err) {
        // Catch all uncatched DB-related errors and exceptions
        console.error("db error err=" + err);
      });
    })
  }

})();
