(function() {
  'use strict';

  angular
    .module('simility')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
